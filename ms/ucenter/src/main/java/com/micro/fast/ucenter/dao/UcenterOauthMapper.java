package com.micro.fast.ucenter.dao;

import com.micro.fast.ucenter.pojo.UcenterOauth;

public interface UcenterOauthMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UcenterOauth record);

    int insertSelective(UcenterOauth record);

    UcenterOauth selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UcenterOauth record);

    int updateByPrimaryKey(UcenterOauth record);
}